package ru.arubtsova.tm.exception.empty;

import ru.arubtsova.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error! Empty Role...");
    }

}
