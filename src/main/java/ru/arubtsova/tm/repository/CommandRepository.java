package ru.arubtsova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.arubtsova.tm.api.repository.ICommandRepository;
import ru.arubtsova.tm.command.AbstractCommand;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CommandRepository implements ICommandRepository {

    @NotNull
    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        initCommands();
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.arubtsova.tm.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.arubtsova.tm.command.AbstractCommand.class)
                .stream()
                .sorted(Comparator.comparing(Class::getName))
                .collect(Collectors.toList());
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            commandList.add(clazz.newInstance());
        }
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommandNames() {
        final List<AbstractCommand> result = new ArrayList<>();
        for (final AbstractCommand command : commandList) {
            final String arg = command.arg();
            if (arg == null || arg.isEmpty()) continue;
            result.add(command);
        }
        return result;
    }

}
