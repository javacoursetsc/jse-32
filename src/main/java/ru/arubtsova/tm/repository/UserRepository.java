package ru.arubtsova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.repository.IUserRepository;
import ru.arubtsova.tm.model.User;

import java.util.Optional;
import java.util.function.Predicate;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    public static Predicate<User> predicateByLogin(@NotNull final String login) {
        return u -> login.equals(u.getLogin());
    }

    @NotNull
    public static Predicate<User> predicateByEmail(@NotNull final String email) {
        return u -> email.equals(u.getEmail());
    }

    @NotNull
    @Override
    public Optional<User> findByLogin(@NotNull final String login) {
        return map.values().stream()
                .filter(predicateByLogin(login))
                .limit(1)
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<User> findByEmail(@NotNull final String email) {
        return map.values().stream()
                .filter(predicateByEmail(email))
                .limit(1)
                .findFirst();
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @NotNull final Optional<User> entity = findByLogin(login);
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

}
